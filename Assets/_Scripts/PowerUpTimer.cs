﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpTimer : MonoBehaviour
{
    // Use this for initialization
    public PlayerController controller;
    public Material progress;
    public Text countDown;
    public static PowerUpTimer staticInst;
    public Color startColor, endColor;
    public GameObject scaler;

    public Vector3 StartPosition;

    // Update is called once per frame

    private void OnEnable()
    {
        StartPosition = transform.localPosition;
        //iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(-200, 0, 0), "time", 1.0f, "easetype", iTween.EaseType.easeInOutBack, "delay", 0.0f, "islocal", true));
    }

    private void destroyTimer(System.Object obj, EventArgs args)
    {
        Destroy(gameObject);
    }

    private void Start()
    {
        staticInst = this;
    }

    public void startCountDown(float timeLength)
    {
        iTween.RotateTo(scaler, new Vector3(0, 0, 0), 0);
        //iTween.MoveTo(gameObject, iTween.Hash("islocal", false, "position", StartPosition, "time", 0.5f, "easetype", iTween.EaseType.easeOutSine));//"isloacal",true

        Hashtable ht = iTween.Hash("from", timeLength, "to", 0, "time", timeLength, "onupdate", "ChangeTextValue", "oncomplete", "timerDone", "islocal", true);
        iTween.ValueTo(gameObject, ht);
    }

    private void ChangeTextValue(float countNumber)
    {
        countDown.text = "" + Mathf.RoundToInt(countNumber);
    }

    private void changeAlpha(float alphaValue)
    {
        progress.SetFloat("_Cutoff", alphaValue);
    }

    void timerDone()
    {
        controller.SwitchOffShield();
    }
}