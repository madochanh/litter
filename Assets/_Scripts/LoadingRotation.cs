﻿using UnityEngine;

public class LoadingRotation : MonoBehaviour
{
    // Use this for initialization

    private void Start()
    {
    }

    private bool canRotate = false;

    private void OnBecameVisible()
    {
        canRotate = true;
    }

    // Update is called once per frame
    private void Update()
    {
        if (canRotate) transform.Rotate(Vector3.forward * -3);
    }
}