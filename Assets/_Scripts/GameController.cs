﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public InGameUIController ingameController;
    public GameObject[] spawnBlock;
    public GameObject[] NewCoins;
    public GameObject[] NewGuns;
    public GameObject[] NewEnemies;
    public GameObject NewZapperPair;
    public GameObject ShieldIcon, MagnetIcon;
    //float Speed = 0.5f;
    public int NextPlatformDistance = 70;
    public int index;
    public static GameController Static;
    public GameObject PlayerObj;
    int Blockcount = 1;
    public bool isGameEnded = false;
    public GameObject Explosion1, Explosion2, Explosion3, Explosion4, Explosion5, Explosion6, SmokeParticle;
    public bool stopGameplay = false;
    public GameObject WorldChanger, Rays, WhiteFadeOut;

    void Start()
    {
        Static = this;

        isGameEnded = false;
        WorldChanger.SetActive(false);
    }

    public void OnGameStart()
    {
        InvokeRepeating("CreateNewCoins", 0.5f, 6.5f);
        InvokeRepeating("CreateNewGuns", 3.3f, 15.0f);
        InvokeRepeating("CreateNewZapperPair", 1.7f, 12.5f);
        InvokeRepeating("CreateNewEnemies", 0.5f, 1.4f);
        InvokeRepeating("CreateNewShieldIcon", 6.5f, 29.5f);
        InvokeRepeating("CreateNewMagnetIcon", 2.5f, 18.5f);
        InvokeRepeating("ChangeWorld", 20, 40);
    }

    public void StopInstantiate()
    {
        isGameEnded = true;
    }

    bool justOnce = false;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && justOnce == false && PlayerController.currentState == PlayerController.PlayerStates.idle)
        {
            ingameController.OnGameStart();
            PlayerController.currentState = PlayerController.PlayerStates.running;

            Camera.main.GetComponent<Camerafollow>().enabled = true;
            OnGameStart();//to invoke enimes 
            justOnce = true;
        }


        if (Input.GetKeyDown(KeyCode.T))
        {

            ChangeWorld();
        }
    }

    public void CreateNewCoins()
    {
        if (isGameEnded || stopGameplay)
            return;

        GameObject obj = GameObject.Instantiate(NewCoins[Random.Range(0, NewCoins.Length - 1)], new Vector3(PlayerObj.transform.position.x + 20, Random.Range(1.8f, 0.9f), 0), Quaternion.identity) as GameObject;
    }

    public void CreateNewEnemies()
    {
        if (isGameEnded || stopGameplay)
            return;
        GameObject randomObj = NewEnemies[Random.Range(0, NewEnemies.Length)];

        if (!randomObj.name.Contains("Type3") && !randomObj.name.Contains("Type5"))
        {
            GameObject obj = GameObject.Instantiate(randomObj, new Vector3(PlayerObj.transform.position.x + 16, 3.9f, 0), Quaternion.identity) as GameObject;
        }
        else
        {
            GameObject.Instantiate(randomObj, new Vector3(PlayerObj.transform.position.x + 18, 1.9f, 0), Quaternion.identity);
        }
    }

    public void CreateNewZapperPair()
    {
        if (isGameEnded || stopGameplay)
            return;

        GameObject obj = GameObject.Instantiate(NewZapperPair, new Vector3(PlayerObj.transform.position.x + Random.Range(15, 25), 0.0f, -0.38f), Quaternion.identity) as GameObject;
    }

    public void CreateNewGuns()
    {
        if (isGameEnded || stopGameplay)
            return;
        print("creating guns");
        GameObject obj = GameObject.Instantiate(NewGuns[Random.Range(0, NewGuns.Length)], new Vector3(PlayerObj.transform.position.x + 15, Random.Range(3.5f, 2.2f), -0.38f), Quaternion.identity) as GameObject;
    }

    public void CreateNewShieldIcon()
    {
        if (isGameEnded || stopGameplay)
            return;
        //GameObject obj= GameObject.Instantiate(NewCoins[Random.Range(0,NewCoins.Length)],new Vector3(0.0f,0.0f,0.0f),Quaternion.identity)as GameObject;
        GameObject obj = GameObject.Instantiate(ShieldIcon, new Vector3(PlayerObj.transform.position.x + 40, Random.Range(3.5f, 2.2f), 0), Quaternion.identity) as GameObject;
    }

    public void CreateNewBlock()
    {
        if (isGameEnded || stopGameplay)
            return;
        index = Random.Range(0, spawnBlock.Length);
        GameObject obj = GameObject.Instantiate(spawnBlock[index], new Vector3(NextPlatformDistance * Blockcount, 0, 0), Quaternion.identity) as GameObject;
        Blockcount++;
    }

    public void CreateNewMagnetIcon()
    {
        if (isGameEnded || stopGameplay)
            return;
        //GameObject obj= GameObject.Instantiate(NewCoins[Random.Range(0,NewCoins.Length)],new Vector3(0.0f,0.0f,0.0f),Quaternion.identity)as GameObject;
        GameObject obj = GameObject.Instantiate(MagnetIcon, new Vector3(PlayerObj.transform.position.x + 60, Random.Range(3.5f, 2.2f), 0), Quaternion.identity) as GameObject;
    }

    void ChangeWorld()
    {
        if (stopGameplay)
            return;
        WorldChanger.SetActive(true);
        stopGameplay = true;
        WorldChanger.transform.position = PlayerObj.transform.position + new Vector3(0, 2, 0);

        iTween.ScaleTo(Rays, iTween.Hash("scale", new Vector3(4, 6, 2), "time", 1.0f, "name", "raysTween"));

        WhiteFadeOut.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0);//starts at zero alpha 
        iTween.FadeTo(WhiteFadeOut, iTween.Hash("alpha", 1, "time", 1.5f));
        Invoke("fadeToZero", 1.5f);
    }

    public void fadeToZero()
    {
        iTween.FadeTo(WhiteFadeOut, iTween.Hash("alpha", 0, "time", 1.5f, "oncomplete", "reseumeWorldGameplay"));
        ingameController.ChangeWorldTextures();

        Invoke("reseumeWorldGameplay", 1.0f);
    }

    public void reseumeWorldGameplay()
    {
        WorldChanger.SetActive(false);
        stopGameplay = false;
    }
}
