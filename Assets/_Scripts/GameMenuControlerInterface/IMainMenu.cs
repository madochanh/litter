﻿using System.Collections;
using UnityEngine;

public interface IMainMenu
{
    void MakeInstance();

    void StartGame();

    void InvokeStartGame();

    void MoreGame();

    void ShareFacebook();

    void RateUs();

    void CanvasState(string name);

    // void DeactiveCanvas(GameObject canvas);

    IEnumerator DeactiveCanvas(GameObject Canvas1, GameObject canvas2, GameObject canvas3, GameObject canvas4);
}