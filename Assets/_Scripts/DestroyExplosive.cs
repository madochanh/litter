﻿using UnityEngine;

public class DestroyExplosive : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
        Destroy(gameObject, 0.6f);
    }
}