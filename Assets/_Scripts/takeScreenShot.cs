﻿using UnityEngine;

public class takeScreenShot : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
        //DontDestroyOnLoad (gameObject);
    }

    private string resolution;

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            Screen.SetResolution(640, 1136, false);
            resolution = "" + Screen.width + "X" + Screen.height;
            Application.CaptureScreenshot("ScreenShot-" + resolution + "-" + PlayerPrefs.GetInt("number", 0) + ".png");
            PlayerPrefs.SetInt("number", PlayerPrefs.GetInt("number", 0) + 1);
            //Debug.Log ("takenShot with " + resolution);
        }
    }
}