﻿using UnityEngine;

public class Zapperscript : MonoBehaviour
{
    public GameObject Zapper1, Zapper2;

    private void Start()
    {
        if (Random.Range(-1, 1) > 0)
        {
            Zapper1.SetActive(true);
            Zapper2.SetActive(false);
        }
        else
        {
            Zapper1.SetActive(false);
            Zapper2.SetActive(true);
        }

        transform.Rotate(0, 0, 90 * Random.Range(0, 4));
    }
}