﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public InGameUIController inGameController;
    // Use this for initialization
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    Vector3 moveDirection;
    public CharacterController playerController;
    public GameObject platfrom, Shield, MagnetPower, ShieldTimeParent;
    public static PlayerController Static;
    int jumpCount = 1;
    public string NameObj;
    public Vector3 PlayerPosition;
    public static Vector3 PlayerCurrentPosition;
    public Transform thisTrans;
    public float playerRunAnimSpeed, playerAttack1AnimSpeed;
    public float textureCount;

    public Texture Gun1Texture, Gun2Texture, Gun3Texture, Gun4Texture, Gun5Texture;
    //	public Texture playerUpTexturechange;
    public Texture[] playerRunCycleTexture, PlayerAttack1Textures, bullettextures;
    public Material bulletIconMaterial;
    Material playerMaterial;
    public Material GunMeterial;
    public static bool isPlayerDead = false;
    public bool isShieldOn = false;
    public bool isMagnet = false;
    public float shieldTime;
    public GameObject playerRenderer;
    public GameObject GunRenderer;
    public bool jump = false;
    public Transform bulletStartTransfrom;
    public GameObject bullet_for_gun1, bullet_for_gun2, bullet_for_gun3, bullet_for_gun4, bullet_for_gun5;
    public GameObject gun5, GunObj, BulletIndicator;
    public TextMesh bulletRemainingCount;
    public bool isPlayerfalling = false;
    //int Bullet1Count = 10;
    int Bullet2Count = 0;
    int Bullet3Count = 0;
    int Bullet4Count = 0;


    public enum PlayerStates
    {
        idle,
        running,
        nojump,
        attack1,
        dead,
        dead2
    }

    public enum GunType
    {
        gun1,
        gun2,
        gun3,
        gun4,
        gun5
    }
    public static PlayerStates currentState;
    public static GunType currentGunType;

    void Start()
    {
        playerController = GetComponent<CharacterController>();
        currentState = PlayerStates.running;
        thisTrans = transform;
        isPlayerDead = false;
        Static = this;
        GunMeterial.mainTexture = Gun1Texture;
        Shield.GetComponent<Renderer>().enabled = false;
        iTween.RotateAdd(Shield, iTween.Hash("amount", new Vector3(0, 0, -360), "time", 0.5f, "looptype", iTween.LoopType.loop));
        iTween.PunchScale(Shield, iTween.Hash("amount", new Vector3(1, 1, 1), "time", 0.5f, "looptype", iTween.LoopType.loop));
        scrollingScripts = GameObject.FindObjectsOfType<UVScrolling>();
        currentGunType = GunType.gun1;
        playerMaterial = playerRenderer.GetComponent<Renderer>().material;
    }

    void PlayerRunAnimation()
    {
        if (textureCount > playerRunCycleTexture.Length - 1)
        {
            textureCount = 0;
        }
        playerMaterial.mainTexture = playerRunCycleTexture[Mathf.RoundToInt(textureCount)];
        textureCount += playerRunAnimSpeed * Time.deltaTime;
    }

    void PlayerAttack1Animation()
    {
        if (textureCount > PlayerAttack1Textures.Length - 1)
        {
            currentState = PlayerStates.running;
        }

        playerMaterial.mainTexture = PlayerAttack1Textures[Mathf.RoundToInt(textureCount)];
        textureCount += playerAttack1AnimSpeed;
    }
    
    // Update is called once per frame
    float runningTime;
    public Vector3 rayOffset;

    void Update()
    {
        PlayerCurrentPosition = thisTrans.position;

        if (GameController.Static.stopGameplay)
        {
            return;
        }

        switch (currentState)
        {
            case PlayerStates.running:
                Vector3 down = transform.TransformDirection(Vector3.down);

                if (Physics.Raycast(thisTrans.position + rayOffset, down, 2))
                {
                    PlayerRunAnimation();
                }

                if (playerController.isGrounded)
                {
                    moveDirection = transform.right;
                    moveDirection = transform.TransformDirection(moveDirection);
                    moveDirection *= speed;
                    jumpCount = 1;
                }

                moveDirection.y -= gravity * Time.deltaTime;
                playerController.Move(moveDirection * Time.deltaTime);
                ScrollBackGround();
                break;

            case PlayerStates.attack1:
                PlayerAttack1Animation();
                break;

            case PlayerStates.dead:
                stopScrollingBackground();
                Playerdead deadScript = gameObject.AddComponent<Playerdead>();
                break;
            case PlayerStates.idle:
                stopScrollingBackground();
                break;
        }
    }

    public void onShoot()
    {
        if (currentState == PlayerStates.running)
        {
            WeaponHandling();
        }
    }

    public void onJump()
    {
        if (currentState == PlayerStates.running && jumpCount <= 2)
        {
            moveDirection.y = jumpSpeed;
            jumpCount++;
            moveDirection.y -= gravity * Time.deltaTime;
            playerController.Move(moveDirection * Time.deltaTime);
        }
    }

    void OnTriggerEnter(Collider incoming)
    {
        NameObj = incoming.GetComponent<Collider>().name;
        if (NameObj.Contains("CreateNewBlockTrigger"))
        {
            GameController.Static.CreateNewBlock();

        }
        else if (NameObj.Contains("Coin"))
        {
            SoundController.Static.PlayCoinsCollectSound();
            if (!isMagnet)
            {
                incoming.enabled = false;
                incoming.GetComponent<Renderer>().enabled = false;
                Destroy(incoming.gameObject);
                inGameController.UpdateInGameCoinCount();
            }
            incoming.GetComponent<CoinsRotation>().isMove = true;
        }

        if (NameObj.Contains("PlayerEntranceTrigger"))
        {
            currentState = PlayerStates.idle;

        }
        else if (NameObj.Contains("Zapper") && !isShieldOn)
        {
            inGameController.HelthCount--;
            iTween.PunchRotation(Camera.main.gameObject, iTween.Hash("amount", new Vector3(0.3f, 0.3f, 0.3f), "time", 0.5f));
        }
        else if (NameObj.Contains("Gun2"))
        {
            if (PlayerPrefs.GetInt("Gun2BulletCount", 0) <= 0)
            {
                Bullet2Count += 10;
            }
            else
            {
                Bullet2Count += 10 * PlayerPrefs.GetInt("Gun2BulletCount", 0);
            }
            Destroy(incoming.gameObject);
            currentGunType = GunType.gun2;
            GunMeterial.mainTexture = Gun2Texture;
            SoundController.Static.Playpowersound();
            BulletIndicator.SetActive(true);
            bulletIconMaterial.mainTexture = bullettextures[0];
            bulletRemainingCount.text = "x" + Bullet2Count;
            inGameController.showPowerGunDisplay(2);
        }
        else if (NameObj.Contains("Gun3"))
        {

            if (PlayerPrefs.GetInt("Gun3BulletCount", 0) <= 0)
            {
                Bullet3Count += 10;
            }
            else
            {
                Bullet3Count += 10 * PlayerPrefs.GetInt("Gun3BulletCount", 0);
            }
            Destroy(incoming.gameObject);
            currentGunType = GunType.gun3;
            GunMeterial.mainTexture = Gun3Texture;
            SoundController.Static.Playpowersound();
            BulletIndicator.SetActive(true);
            bulletIconMaterial.mainTexture = bullettextures[1];
            bulletRemainingCount.text = "x" + Bullet3Count;
            inGameController.showPowerGunDisplay(3);
        }
        else if (NameObj.Contains("Gun4"))
        {

            print("" + PlayerPrefs.GetInt("Gun4BulletCount", 0));
            if (PlayerPrefs.GetInt("Gun4BulletCount", 0) <= 0)
            {
                Bullet4Count += 10;
            }
            else
            {
                Bullet4Count += 10 * PlayerPrefs.GetInt("Gun4BulletCount", 0);
            }
            Destroy(incoming.gameObject);
            currentGunType = GunType.gun4;
            GunMeterial.mainTexture = Gun4Texture;
            SoundController.Static.Playpowersound();
            BulletIndicator.SetActive(true);
            bulletIconMaterial.mainTexture = bullettextures[2];
            bulletRemainingCount.text = "x" + Bullet4Count;
            inGameController.showPowerGunDisplay(4);
        }
        else if (NameObj.Contains("Gun5"))
        {
            BulletIndicator.SetActive(false);
            Destroy(incoming.gameObject);
            currentGunType = GunType.gun5;
            GunMeterial.mainTexture = Gun5Texture;
            SoundController.Static.Playpowersound();
            inGameController.showPowerGunDisplay(5);
        }
        else if (NameObj.Contains("MagnetPickUp"))
        {
            incoming.enabled = false;
            incoming.GetComponent<Renderer>().enabled = false;
            Destroy(incoming.gameObject);
            isMagnet = true;
            ActiveMagnetPower();
            SoundController.Static.Playpowersound();
        }
        else if (NameObj.Contains("ShiledPickUp") && !isShieldOn)
        {
            ActivateShield();
            incoming.enabled = false;
            incoming.GetComponent<Renderer>().enabled = false;
            Destroy(incoming.gameObject);
            //isShieldOn = true;
            ShieldTimeParent.SetActive(true);
            ShieldTimeParent.GetComponent<PowerUpTimer>().startCountDown(20f);
            //PowerUpTimer.staticInst.startCountDown(20f);
            SoundController.Static.Playpowersound();
        }
    }

    public void SwitchOffShield()
    {
        //Shield.GetComponent<Renderer>().enabled = false;
        isShieldOn = false;
        Shield.GetComponent<Renderer>().enabled = false;
        ShieldTimeParent.SetActive(false);
    }

    public void ActivateShield()
    {
        isShieldOn = true;
        Shield.GetComponent<Renderer>().enabled = true;
        Invoke("SwitchOffShield", 20f);
    }

    public void ActiveMagnetPower()
    {
        MagnetPower.SetActive(true);
        Invoke("DeactiveMagnetPower", 15 + PlayerPrefs.GetInt("MagnetCount", 0));
    }

    void DeactiveMagnetPower()
    {
        isMagnet = false;
        MagnetPower.SetActive(false);
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        string collidedName = hit.collider.name;

        if (collidedName.Contains("GameOverCollider"))
        {
            inGameController.ongameEnded();
            isPlayerDead = true;
            currentState = PlayerStates.dead;
        }
        if (collidedName.Contains("Enemy") && !isShieldOn)
        {
            if (thisTrans.position.y > hit.collider.transform.position.y)
            {
                Destroy(hit.collider.gameObject);
                GameObject.Instantiate(GameController.Static.SmokeParticle, hit.collider.transform.position, Quaternion.identity);
                inGameController.killCount++;
                SoundController.Static.PlayEmemyDead();
            }
            else
            {
                isPlayerDead = true;
                hit.collider.isTrigger = true;
                inGameController.HelthCount--;
                iTween.PunchRotation(Camera.main.gameObject, iTween.Hash("amount", new Vector3(0.5f, 0.5f, 0.5f), "time", 0.5f));
            }
        }
        else if (collidedName.Contains("Enemy") && isShieldOn)
        {
            EnemyScript enmyScript = hit.collider.GetComponent<EnemyScript>();
            if (enmyScript != null)
                enmyScript.enabled = false;
            EnemyDeadScript deadScript = hit.collider.gameObject.AddComponent<EnemyDeadScript>();
            hit.collider.name = "used";
            inGameController.killCount++;
            SoundController.Static.PlayEmemyDead();
        }
    }

    void WeaponHandling()
    {
        switch (currentGunType)
        {
            case GunType.gun1:
                BulletIndicator.SetActive(false);
                SoundController.Static.PlayBullet1Sound();
                iTween.PunchPosition(GunObj, iTween.Hash("Amount", new Vector3(0.2f, 0.1f, 0.0f), "time", 0.3f, "Eesetype", iTween.EaseType.easeInOutBack));

                GameObject bulletObj1 = GameObject.Instantiate(bullet_for_gun1, bulletStartTransfrom.position, Quaternion.identity) as GameObject;
                Destroy(bulletObj1, 3); //to destroy bullect after 3 seconds 
                break;
            case GunType.gun2:

                BulletIndicator.SetActive(true);
                SoundController.Static.PlayBullet2Sound();
                iTween.PunchPosition(GunObj, iTween.Hash("Amount", new Vector3(0.2f, 0.1f, 0.0f), "time", 0.3f, "Eesetype", iTween.EaseType.easeInOutBack));
                GameObject bulletObj2 = GameObject.Instantiate(bullet_for_gun2, bulletStartTransfrom.position, Quaternion.identity) as GameObject;
                Destroy(bulletObj2, 3);
                if (Bullet2Count <= 1)
                {
                    currentGunType = GunType.gun1;
                    GunMeterial.mainTexture = Gun1Texture;
                }
                Bullet2Count--;

                bulletIconMaterial.mainTexture = bullettextures[0];
                bulletRemainingCount.text = "x" + Bullet2Count;
                break;
            case GunType.gun3:
                BulletIndicator.SetActive(true);

                SoundController.Static.PlayBullet3Sound();
                iTween.PunchPosition(GunObj, iTween.Hash("Amount", new Vector3(0.2f, 0.1f, 0.0f), "time", 0.3f, "Eesetype", iTween.EaseType.easeInOutBack));
                GameObject bulletObj3 = GameObject.Instantiate(bullet_for_gun3, bulletStartTransfrom.position, Quaternion.identity) as GameObject;
                Destroy(bulletObj3, 3);
                Bullet3Count--;
                if (Bullet3Count <= 1)
                {
                    currentGunType = GunType.gun1;
                    GunMeterial.mainTexture = Gun1Texture;
                }

                bulletIconMaterial.mainTexture = bullettextures[1];
                bulletRemainingCount.text = "x" + Bullet3Count;
                break;
            case GunType.gun4:
                BulletIndicator.SetActive(true);

                print("gun 4 activated here ");
                SoundController.Static.PlayBullet4Sound();
                iTween.PunchPosition(GunObj, iTween.Hash("Amount", new Vector3(0.2f, 0.1f, 0.0f), "time", 0.3f, "Eesetype", iTween.EaseType.easeInOutBack));
                GameObject bulletObj4 = GameObject.Instantiate(bullet_for_gun4, bulletStartTransfrom.position, Quaternion.identity) as GameObject;
                if (Bullet4Count <= 1)
                {
                    print("gun 4 deactivated here ");
                    currentGunType = GunType.gun1;
                    GunMeterial.mainTexture = Gun1Texture;
                    print("gun changed here ");
                }
                Bullet4Count--;
                bulletRemainingCount.text = "x" + Bullet4Count;
                bulletIconMaterial.mainTexture = bullettextures[2];

                break;
            case GunType.gun5:
                BulletIndicator.SetActive(false);
                iTween.PunchPosition(GunObj, iTween.Hash("Amount", new Vector3(0.2f, 0.1f, 0.0f), "time", 0.3f, "Eesetype", iTween.EaseType.easeInOutBack));
                gun5.SetActive(true);
                SoundController.Static.PlayLaserfireSound();
                Invoke("DeactivateLaser", 1.5f + PlayerPrefs.GetInt("Gun4BulletCount", 0));
                break;
        }
    }

    public UVScrolling[] scrollingScripts;

    public void ScrollBackGround()
    {
        foreach (UVScrolling script in scrollingScripts)
        {
            script.enabled = true;
        }
    }

    public void stopScrollingBackground()
    {
        foreach (UVScrolling script in scrollingScripts)
        {
            script.enabled = false;
        }
    }

    public void DeactivateLaser()
    {
        gun5.SetActive(false);
        currentGunType = GunType.gun1;
        GunMeterial.mainTexture = Gun1Texture;
    }
}
