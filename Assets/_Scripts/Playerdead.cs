﻿using UnityEngine;

public class Playerdead : MonoBehaviour
{
    public float Speed = 0.000001f;

    private void Start()
    {
        //PlayerFalldown ();
        //iTween.MoveTo(gameObject, iTween.Hash("position", transform.position + new Vector3(1, -2, 0), "time", 0.2f, "oncomplete", "playerFalling"));
        Invoke ("playerFalling", 0.5f);
        //gameObject.name="Dead";
    }

    private void playerFalling()
    {
        Invoke("Dead", 2f);
        //print("Playerfalling method calling here ");
        //iTween.MoveTo(gameObject, iTween.Hash("position", transform.position + Vector3.up * -2, "time", 0.2f));
    }

    private void Dead()
    {
        Destroy(gameObject);
    }
}