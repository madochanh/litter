﻿using UnityEngine;

public class TweenTest : MonoBehaviour
{
    // Use this for initialization

    private void Start()
    {
        Time.timeScale = 0;
        iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(2, 0, 0), "time", 1.0f, "ignoretimescale", true));
    }

    // Update is called once per frame
    private void Update()
    {
    }
}