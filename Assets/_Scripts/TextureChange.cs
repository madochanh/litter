﻿using UnityEngine;

public class TextureChange : MonoBehaviour
{
    public Texture[] GameObjTextrues;
    public Material GameObjMaterial;
    public float textureCount;
    public float animationSpeed;

    private void Update()
    {
        TextureChangeForGameObj();
    }

    private void TextureChangeForGameObj()
    {
        if (textureCount > GameObjTextrues.Length - 1)
            textureCount = 0;

        GameObjMaterial.mainTexture = GameObjTextrues[Mathf.RoundToInt(textureCount)];
        textureCount += animationSpeed * Time.deltaTime;
    }
}