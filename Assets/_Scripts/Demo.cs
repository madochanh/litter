﻿using GoogleMobileAds.Api;
using System;
using UnityEngine;

//using UnityEngine.Advertisements;

//using UnityEngine.Advertisements;

#if UNITY_ADS

using UnityEngine.Advertisements; // only compile Ads code on supported platforms

#endif

public class Demo : MonoBehaviour
{
    public static Demo _instance;
    private BannerView bannerview;
    private RewardBasedVideoAd rewardBasedVideo;
    private InterstitialAd interstitial;

    private bool CheckInternet;
    private const string rewardedVideo = "rewardedVideo";

    // Use this for initialization
    private void Awake()
    {
        //    ShowRewardedAd();
        //  RequestRewardBasedVideo();
        if (_instance == null)
        {
            _instance = this;
        }
    }

    private void Start()
    {
        requestBanner();
        requestInterestitial();

        showBanner();
    }

    // Update is called once per frame

    public void requestBanner()
    {
        string idBanner = "";
#if UNITY_ANDROID
        idBanner = "ca-app-pub-9539808417984679/4221559744";
#endif
#if UNITY_IPHONE || UNITY_IOS
        idBanner = "ca-app-pub-6102026202830298/2023447564";
#endif

        bannerview = new BannerView(idBanner, AdSize.SmartBanner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();
        bannerview.LoadAd(request);
        bannerview.OnAdClosed += Bannerview_AdClosed;
    }

    private void Bannerview_AdClosed(object sender, EventArgs e)
    {
        if (bannerview != null)
        {
            interstitial.Destroy();
        }
        requestBanner();
    }

    public void showBanner()
    {
        bannerview.Show();
    }

    public void hideBanner()
    {
        bannerview.Hide();
    }

    public void requestInterestitial()
    {
#if UNITY_ANDROID
        interstitial = new InterstitialAd("ca-app-pub-9539808417984679/2744826542");
#endif
#if UNITY_IPHONE || UNITY_IOS
        interstitial = new InterstitialAd("ca-app-pub-6102026202830298/3500180763");
#endif

        AdRequest request = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);
        interstitial.OnAdClosed += HandClosed;
    }

    public void showIntertitial()
    {
        if (interstitial.IsLoaded())
        {
            //  Debug.Log("Show");
            interstitial.Show();
        }
        //else
        //{
        //    CheckInternet = true;
        //}
    }

    public void HandClosed(object sender, EventArgs arg)
    {
        if (interstitial != null)
        {
            interstitial.Destroy();
        }
        requestInterestitial();
        //CheckNoloadVideoDialog.SetActive(false);
        //ShowBugBanner();
    }

    private bool showBigBanner;

    private void RequestRewardBasedVideo()
    {
#if UNITY_EDITOR
        string adUnitId = "ca-app-pub-9539808417984679/1125211741";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-5115698502374820/5186451197";
#elif UNITY_IPHONE
        string adUnitId = "INSERT_AD_UNIT_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif
        // RewardBasedVideoAd mRewardvideo = GoogleMobileAds
        rewardBasedVideo = RewardBasedVideoAd.Instance;

        AdRequest request = new AdRequest.Builder().Build();
        rewardBasedVideo.LoadAd(request, adUnitId);

        //  rewardBasedVideo.OnAdFailedToLoad += RewardBasedVideo_OnAdFailedToLoad;

        rewardBasedVideo.OnAdRewarded += RewardBasedVideo_OnAdRewarded;
    }

    private void RewardBasedVideo_OnAdRewarded(object sender, Reward e)
    {
    }
}

//disable notice load