﻿using UnityEngine;

public class laserScaling : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
        iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(23, 2, 0.7f), "time", 0.7f, "looptype", iTween.LoopType.pingPong));
    }
}