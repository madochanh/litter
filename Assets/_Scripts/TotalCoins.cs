﻿using UnityEngine;
using UnityEngine.UI;

public class TotalCoins : MonoBehaviour
{
    public static TotalCoins Static;
    public Text TotalCoinstext;
    private int coinsIn;

    private void Start()
    {
        UpdateCoins();
        Static = this;
        // PlayerPrefs.SetInt("TotalCoins", 10000000);
    }

    public void UpdateCoins()
    {
        TotalCoinstext.text = ":" + PlayerPrefs.GetInt("TotalCoins", 0);
    }

    public void AddCoins(int AddingCoins)
    {
        PlayerPrefs.SetInt("TotalCoins", PlayerPrefs.GetInt("TotalCoins", 0) + AddingCoins);
        UpdateCoins();
    }

    public void SubtractCoins(int SubtractingCoins)
    {
        PlayerPrefs.SetInt("TotalCoins", PlayerPrefs.GetInt("TotalCoins", 0) - SubtractingCoins);
        UpdateCoins();
    }

    public int getCoinCount()
    {
        return PlayerPrefs.GetInt("TotalCoins", 0);
    }
}