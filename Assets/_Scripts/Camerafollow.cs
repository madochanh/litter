﻿using UnityEngine;

public class Camerafollow : MonoBehaviour
{
    // Use this for initialization
    public Transform Target;

    public Vector3 Offset;
    public Transform thisTranform;

    private void Start()
    {
        thisTranform = transform;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        float posY = Offset.y;
        posY = Mathf.Clamp(posY, 0, Target.position.y);

        thisTranform.position = new Vector3(Target.position.x + Offset.x, Offset.y, Offset.z);
    }
}