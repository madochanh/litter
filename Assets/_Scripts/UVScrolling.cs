﻿using UnityEngine;

public class UVScrolling : MonoBehaviour
{
    public int materialIndex = 0;
    public Vector2 uvAnimationRate = new Vector2(1.0f, 0.0f);
    public string textureName = "_MainTex";

    private Vector2 uvOffset = Vector2.zero;
    public bool overrideStop = false;
    public Material bgMaterial;

    void LateUpdate()
    {
        if (GameController.Static != null && GameController.Static.stopGameplay && !overrideStop)
        {
            return;
        }

        uvOffset += (uvAnimationRate * Time.deltaTime);

        if (GetComponent<Renderer>().enabled && bgMaterial == null)
        {
            GetComponent<Renderer>().materials[materialIndex].SetTextureOffset(textureName, uvOffset);
        }
        else
        {
            bgMaterial.SetTextureOffset(textureName, uvOffset);
        }
    }
}