﻿using UnityEngine;

public class LoadingAnimation : MonoBehaviour
{
    public float LoadingAnimationSpeed = 0.1f;
    public float textureCount;
    public Texture[] LoadingAnimationTexture;
    public Material LoadingMaterial;

    private void Start()
    {
        //	LoadingMaterial = renderer.material;
    }

    // Update is called once per frame
    private void Update()
    {
        Loading();
    }

    public void Loading()
    {
        if (textureCount > LoadingAnimationTexture.Length - 1)
            textureCount = 0;
        LoadingMaterial.mainTexture = LoadingAnimationTexture[Mathf.RoundToInt(textureCount)];
        textureCount += LoadingAnimationSpeed;
    }
}