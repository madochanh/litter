﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SpalshScreen : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
        Invoke("loadMenuLevel", 2.0f);
        //iTween.FadeTo(logo, 1.0f, 1.0f);
        //Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    // Update is called once per frame

    private void loadMenuLevel()
    {
        SceneManager.LoadSceneAsync("Mainmenu");
    }

    //public GameObject logo;

    private void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            loadMenuLevel();
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}