﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainuMenuController : MonoBehaviour, IMainMenu
{
    public static MainuMenuController _instance;
    public Animator _anim;

    public GameObject Credit, Menu, Shop, Loading;

    private void Awake()
    {
        MakeInstance();
    }

    public void MakeInstance()
    {
        if (_instance == null)
        {
            _instance = this;
        }
    }

    public void InvokeStartGame()
    {
        Demo._instance.hideBanner();
        //ADSController._instance.HideBannerView();
        SoundController.Static.PlayClickSound();
        Invoke("StartGame", 1f);
    }

    public void StartGame()
    {
        SceneManager.LoadSceneAsync("gameplay");
    }

    public void MoreGame()
    {
        SoundController.Static.PlayClickSound();
        Application.OpenURL("Link");
    }

    public void ShareFacebook()
    {
        SoundController.Static.PlayClickSound();
        Debug.Log("ShareFacebook");
    }

    public void CanvasState(string name)
    {
        bool isStateAnim;
        if (_anim.GetBool(name) == true)
        {
            isStateAnim = false;
        }
        else
        {
            isStateAnim = true;
        }

        _anim.SetBool(name, isStateAnim);

        SoundController.Static.PlayClickSound();
        // check state menu for deactive canvas object
        switch (name)
        {
            case "Menu":
                if (isStateAnim == true)
                {
                    StartCoroutine(DeactiveCanvas(Menu, Credit, Shop, Loading));
                }
                else
                {
                    StartCoroutine(DeactiveCanvas(Credit, Shop, Menu, Loading));
                }
                break;

            case "Credit":
                if (isStateAnim == true)
                {
                    StartCoroutine(DeactiveCanvas(Menu, Shop, Credit, Loading));
                }
                else
                {
                    StartCoroutine(DeactiveCanvas(Credit, Shop, Menu, Loading));
                }
                break;

            case "Loading":
                if (isStateAnim == true)
                {
                    StartCoroutine(DeactiveCanvas(Menu, Shop, Loading, Credit));
                }
                else
                {
                    StartCoroutine(DeactiveCanvas(Loading, Shop, Menu, Credit));
                }
                break;

            default:
                break;
        }
    }

    public void RateUs()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.NVT.AnimalRush");
    }

    //deactive canvas when next animation
    public IEnumerator DeactiveCanvas(GameObject canvas1, GameObject canvas2, GameObject canvas3, GameObject canvas4)
    {
        canvas3.SetActive(true);
        yield return new WaitForSeconds(0.5f);

        canvas1.SetActive(false);
        canvas2.SetActive(false);
        canvas4.SetActive(false);
    }
}