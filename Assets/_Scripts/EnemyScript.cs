﻿using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public float speed = 0.2f;

    //public GameObject Enemy;
    public Transform thisTrans;

    private Vector3 originalScale;
    private int inverseScale = 1;
    public Texture[] EnemyRunCycleTexture;

    //public CharacterController EnemyController;
    //Vector3 moveDirection;
    public float gravity = 20.0F;

    public float EnemyRunAnimSpeed;
    public float textureCount;
    public Material EnemyMaterial;
    public bool isLimitsApplicable = false;
    public int health = 2;

    private void Start()
    {
        //EnemyController = GetComponent<CharacterController> ();
        thisTrans = transform;
        originalScale = thisTrans.localScale;
    }

    private void EnemyAnimation()
    {
        if (textureCount > EnemyRunCycleTexture.Length - 1)
            textureCount = 0;
        EnemyMaterial.mainTexture = EnemyRunCycleTexture[Mathf.RoundToInt(textureCount)];
        textureCount += EnemyRunAnimSpeed;
    }

    // Update is called once per frame
    private void Update()
    {
        if (PlayerController.currentState == PlayerController.PlayerStates.dead)
        {
            Destroy(gameObject);
        }

        if(thisTrans.position.x < -EnemyRunCycleTexture[0].width * 1.2f)
        {
            Destroy(gameObject);
        }

        if(thisTrans.position.y < -EnemyRunCycleTexture[0].height * 1.2f)
        {
            Destroy(gameObject);
        }

        thisTrans.Translate(-Vector3.right * speed * Time.deltaTime);
        EnemyAnimation();
    }

    private void OnTriggerEnter(Collider hit)
    {
        if (hit.name.Contains("limits") && isLimitsApplicable)
        {
            //print ("Trigger "+hit.name);
            inverseScale *= -1;
            thisTrans.localScale = new Vector3(originalScale.x * inverseScale, originalScale.y, originalScale.z);
            speed *= -1;
        }
        else if (hit.name.Contains("laser_gun5"))
        {
            Destroy(gameObject);
            InGameUIController.Static.killCount++;
        }
    }
}