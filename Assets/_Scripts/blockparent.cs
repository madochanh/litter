﻿using System.Collections.Generic;
using UnityEngine;

public class blockparent : MonoBehaviour
{
    public List<Transform> onlyPlatfroms = new List<Transform>();

    private void Start()
    {
        foreach (Transform child in gameObject.GetComponentsInChildren<Transform>())
        {
            if (child.name.Contains("plat"))
            {
                onlyPlatfroms.Add(child);
            }
        }

        //		onlyPlatfroms [Random.Range(0,onlyPlatfroms.Count)].gameObject.AddComponent<WallDownMovement> ();
    }
}