﻿using UnityEngine;

public class EnemyDeadScript : MonoBehaviour
{
    public float Speed = 0.5f;

    private void Start()
    {
        EnemyFalldown();
        gameObject.name = "Dead";
    }

    public void EnemyFalldown()
    {
        GetComponent<Collider>().enabled = false;

        iTween.MoveTo(gameObject, iTween.Hash("position", transform.position + new Vector3(15, 4.0f, 0), "time", 0.2f, "oncomplete", "deactivate"));
        iTween.RotateAdd(gameObject, iTween.Hash("amount", new Vector3(0, 0, -360), "time", 1.5f));
    }

    private void deactivate()
    {
        print("deactivate function calling");
        iTween.MoveTo(gameObject, iTween.Hash("position", transform.position + new Vector3(10, -7.0f, 0), "time", 0.1f, "oncomplete", "deactivate"));
        Destroy(gameObject);
    }
}