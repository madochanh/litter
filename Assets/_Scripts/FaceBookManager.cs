﻿using Facebook.MiniJSON;
using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FaceBookManager : MonoBehaviour
{
    private List<string> readPermission = new List<string>() { "public_profile", "user_friends", "user_games_activity" },
        publishPermission = new List<string>() { "publish_actions" };

    public string androidLinkShare, iosLinkShare;

    public string rateLink;

    public enum STATE
    {
        SHARE,
        INVITE
    }

    ;

    private STATE currentState;
    private static FaceBookManager _instance;
    public static FaceBookManager Instance { get { return _instance; } }

    private void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    private void Start()
    {
        InitFB();
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void InitFB()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, onHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            //PrintLog("Initialized !");
        }
        else
        {
            //PrintLog("Failed to Initialize the Facebook SDK!");
        }
    }

    // Perform Unity Tasks When App is Connecting To Facebook
    private void onHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    public void ShareOnFB()
    {
        currentState = STATE.SHARE;
        string shareLink = "";

#if UNITY_IOS
		shareLink = iosLinkShare;
#endif

#if UNITY_ANDROID
        shareLink = androidLinkShare;
#endif

        if (FB.IsLoggedIn)
        {
            FB.ShareLink(
                contentURL: new Uri(shareLink),
                callback: delegate (IShareResult shareRes)
                {
                    if (string.IsNullOrEmpty(shareRes.Error) && !shareRes.Cancelled)
                    {
                        Debug.Log("Posting Successful!");
                    }
                    else
                        Debug.Log("Posting Unsuccessful!");
                }
            );
        }
        else
            LoginFB();
    }

    private void LoginFB()
    {
        if (FB.IsLoggedIn)
        {
            Debug.Log("Logged In !");
        }
        else
        {
            FB.ActivateApp();
            FB.LogInWithReadPermissions(readPermission, LoginCallback);
        }
    }

    //Callback method of login
    private void LoginCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            Debug.Log("Logged In Successfully!");
            if (currentState == STATE.SHARE)
                ShareOnFB();
            else if (currentState == STATE.INVITE)
                NativeInviteFriendsFB();
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

    // Native Invite!
    public void NativeInviteFriendsFB()
    {
        currentState = STATE.SHARE;

        if (FB.IsLoggedIn)
        {
            FB.AppRequest(
                "Let's play zombie street trsigger", null, null, null, null, null, "Let's play",
                callback: delegate (IAppRequestResult result)
                {
                    if (result.RawResult != null)
                    {
                        if (DeserializeJSONFriends(result.RawResult) != "")
                            if (GetFriendList(DeserializeJSONFriends(result.RawResult)).Count > 0)
                            {
                                if (PlayerPrefs.GetInt("Invite") == 0)
                                {
                                }
                            }
                    }
                });
        }
        else
            LoginFB();
    }

    public void Rate()
    {
        Application.OpenURL(rateLink);
    }

    public string DeserializeJSONFriends(string response)
    {
        string friendID = "";

        var dict = Json.Deserialize(response) as IDictionary;
        friendID = dict["to"].ToString();

        return friendID;
    }

    private List<string> GetFriendList(string _friend)
    {
        List<string> friendList = new List<string>();
        if (_friend.Contains(","))
        {
            string[] _str = _friend.Split(',');
            for (int i = 0; i < _str.Length; i++)
            {
                friendList.Add(_str[i]);
            }
        }
        else
            friendList.Add(_friend);
        return friendList;
    }
}