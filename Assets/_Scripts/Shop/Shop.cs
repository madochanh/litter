﻿using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour, IShop
{
    public int ShieldCost = 200;
    public int MagnetCost = 100;
    public int Gun2BulletCost = 100;
    public int Gun3BulletCost = 200;
    public int Gun4BulletCost = 300;
    public int Gun5BulletCost = 400;
    public GameObject insufficentCoins;
    public Text ShieldCountTxt, Gun2BulletCount, Gun3BulletCount, Gun4BulletCount, Gun5BulletCount;
    public GameObject MagnetPower1, MagnetPower2, MagnetPower3, MagnetPower4;
    private int count;

    // Use this for initialization
    private void Start()
    {
        Gun2BulletCount.text = "" + PlayerPrefs.GetInt("Gun2BulletCount", 0);
        Gun3BulletCount.text = "" + PlayerPrefs.GetInt("Gun3BulletCount", 0);
        Gun4BulletCount.text = "" + PlayerPrefs.GetInt("Gun4BulletCount", 0);
        Gun5BulletCount.text = "" + PlayerPrefs.GetInt("Gun5BulletCount", 0);

        ShieldCountTxt.text = "" + PlayerPrefs.GetInt("ShieldCount", 0);

        //	print (PlayerPrefs.GetInt ("MagnetCount", 0));

        switch (PlayerPrefs.GetInt("MagnetCount", 0))
        {
            case 0:
                MagnetPower1.SetActive(false);
                MagnetPower2.SetActive(false);
                MagnetPower3.SetActive(false);
                MagnetPower4.SetActive(false);

                break;

            case 1:
                MagnetPower1.SetActive(true);
                MagnetPower2.SetActive(false);
                MagnetPower3.SetActive(false);
                MagnetPower4.SetActive(false);

                break;

            case 2:
                MagnetPower1.SetActive(true);
                MagnetPower2.SetActive(true);
                MagnetPower3.SetActive(false);
                MagnetPower4.SetActive(false);

                break;

            case 3:
                MagnetPower1.SetActive(true);
                MagnetPower2.SetActive(true);
                MagnetPower3.SetActive(true);
                MagnetPower4.SetActive(false);

                break;

            case 4:
                MagnetPower1.SetActive(true);
                MagnetPower2.SetActive(true);
                MagnetPower3.SetActive(true);
                MagnetPower4.SetActive(true);

                break;
        }
    }

    public void BuyShield()
    {
        if (TotalCoins.Static.getCoinCount() >= ShieldCost)
        {
            PlayerPrefs.SetInt("ShieldCount", PlayerPrefs.GetInt("ShieldCount", 0) + 1);
            ShieldCountTxt.text = "" + PlayerPrefs.GetInt("ShieldCount", 0);
            TotalCoins.Static.SubtractCoins(ShieldCost);
        }
        else
        {
            insufficentCoins.SetActive(true);
        }
    }

    public void BuyMagnet()
    {
        if (TotalCoins.Static.getCoinCount() >= MagnetCost && PlayerPrefs.GetInt("MagnetCount", 0) <= 3)
        {
            print("Magnet activated");

            PlayerPrefs.SetInt("MagnetCount", PlayerPrefs.GetInt("MagnetCount") + 1);
            TotalCoins.Static.SubtractCoins(MagnetCost);
            Debug.Log(PlayerPrefs.GetInt("MagnetCount"));

            switch (PlayerPrefs.GetInt("MagnetCount", 0))
            {
                case 0:
                    MagnetPower1.SetActive(false);
                    MagnetPower2.SetActive(false);
                    MagnetPower3.SetActive(false);
                    MagnetPower4.SetActive(false);
                    //PlayerPrefs.SetInt("MagnetCount",PlayerPrefs.GetInt("MagnetCount",0)+10);
                    break;

                case 1:
                    MagnetPower1.SetActive(true);
                    MagnetPower2.SetActive(false);
                    MagnetPower3.SetActive(false);
                    MagnetPower4.SetActive(false);
                    //PlayerPrefs.SetInt("MagnetCount",PlayerPrefs.GetInt("MagnetCount",10)+10);
                    break;

                case 2:
                    MagnetPower1.SetActive(true);
                    MagnetPower2.SetActive(true);
                    MagnetPower3.SetActive(false);
                    MagnetPower4.SetActive(false);
                    //PlayerPrefs.SetInt("MagnetCount",PlayerPrefs.GetInt("MagnetCount",10)+10);
                    break;

                case 3:
                    MagnetPower1.SetActive(true);
                    MagnetPower2.SetActive(true);
                    MagnetPower3.SetActive(true);
                    MagnetPower4.SetActive(false);
                    //PlayerPrefs.SetInt("MagnetCount",PlayerPrefs.GetInt("MagnetCount",10)+10);
                    break;

                case 4:
                    MagnetPower1.SetActive(true);
                    MagnetPower2.SetActive(true);
                    MagnetPower3.SetActive(true);
                    MagnetPower4.SetActive(true);
                    //PlayerPrefs.SetInt("MagnetCount",PlayerPrefs.GetInt("MagnetCount",10)+10);
                    break;
            }
        }
        else
        {
            insufficentCoins.SetActive(true);
        }
    }

    public void BuyGun2()
    {
        if (TotalCoins.Static.getCoinCount() >= Gun2BulletCost)
        {
            if (PlayerPrefs.GetInt("Gun2BulletCount", 0) == 5)
                return;
            PlayerPrefs.SetInt("Gun2BulletCount", PlayerPrefs.GetInt("Gun2BulletCount", 1) + 1);
            Gun2BulletCount.text = "" + PlayerPrefs.GetInt("Gun2BulletCount", 0) * 10;
            TotalCoins.Static.SubtractCoins(Gun2BulletCost);
        }
        else
        {
            insufficentCoins.SetActive(true);
        }
    }

    public void BuyGun3()
    {
        if (TotalCoins.Static.getCoinCount() >= Gun3BulletCost)
        {
            if (PlayerPrefs.GetInt("Gun3BulletCount", 0) == 5)
                return;
            PlayerPrefs.SetInt("Gun3BulletCount", PlayerPrefs.GetInt("Gun3BulletCount", 1) + 1);
            Gun3BulletCount.text = "" + PlayerPrefs.GetInt("Gun3BulletCount", 0) * 10;
            TotalCoins.Static.SubtractCoins(Gun3BulletCost);
        }
        else
        {
            insufficentCoins.SetActive(true);
        }
    }

    public void BuyGun4()
    {
        if (TotalCoins.Static.getCoinCount() >= Gun5BulletCost)
        {
            if (PlayerPrefs.GetInt("Gun4BulletCount", 0) == 5)
                return;
            PlayerPrefs.SetInt("Gun4BulletCount", PlayerPrefs.GetInt("Gun4BulletCount", 1) + 1);
            Gun4BulletCount.text = "" + PlayerPrefs.GetInt("Gun4BulletCount", 0) * 10;
            TotalCoins.Static.SubtractCoins(Gun4BulletCost);
        }
        else
        {
            insufficentCoins.SetActive(true);
        }
    }

    public void BuyGun5()
    {
        if (TotalCoins.Static.getCoinCount() >= Gun5BulletCost)
        {
            if (PlayerPrefs.GetInt("Gun5BulletCount", 0) == 5)
                return;
            PlayerPrefs.SetInt("Gun5BulletCount", PlayerPrefs.GetInt("Gun5BulletCount", 1) + 1);
            Gun5BulletCount.text = "" + PlayerPrefs.GetInt("Gun5BulletCount", 0) * 10;
            TotalCoins.Static.SubtractCoins(Gun5BulletCost);
        }
        else
        {
            insufficentCoins.SetActive(true);
        }
    }

    public void ClostButton()
    {
        insufficentCoins.SetActive(false);
    }
}