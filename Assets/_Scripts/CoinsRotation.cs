﻿using UnityEngine;

public class CoinsRotation : MonoBehaviour
{
    public bool isMove = false;
    public BoxCollider coinsCollider;
    private Transform transformthis;
    public static int totalCoinCount;
    private Vector3 offset;

    private void Start()
    {
        coinsCollider = gameObject.GetComponent<BoxCollider>() as BoxCollider;
        totalCoinCount++;
        transformthis = transform;
        isMove = false;
        coinsCollider.isTrigger = true;
        offset = new Vector3(0, 2, 0);
    }

    // Update is called once per frame
    private bool canRotate = false;

    private bool addOnce = false;

    private void Update()
    {
        if (!canRotate)
            return;
        transformthis.Rotate(new Vector3(0, Time.deltaTime * 180, 0), Space.World);

        if (PlayerController.Static.isMagnet && !isMove)
        {
            coinsCollider.size = new Vector3(25, 35, 30);
        }
        else if (!isMove)
        {
            coinsCollider.size = new Vector3(1, 1, 1);
        }
        if (isMove)
        {
            transformthis.position = Vector3.MoveTowards(transformthis.position, PlayerController.PlayerCurrentPosition + offset, 0.4f);

            if (Vector3.Distance(transformthis.position, PlayerController.PlayerCurrentPosition + offset) < 0.5f)
            {
                isMove = false;
                InGameUIController.Static.CollectedCoins++;
                OnBecameInvisible();
            }

            if (!addOnce)
            {
                addTrailRender();
                addOnce = true;
            }
        }
    }

    private void addTrailRender()
    {
        TrailRenderer trail = gameObject.AddComponent<TrailRenderer>();

        trail.sharedMaterial = InGameUIController.Static.trailRender;
        trail.time = 0.09f;
        trail.startWidth = 0.3f;
        trail.endWidth = 0.1f;
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnBecameVisible()
    {
        canRotate = true;
    }
}