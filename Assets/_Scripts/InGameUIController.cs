﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
 
public class InGameUIController : MonoBehaviour
{
    public static InGameUIController Static;
    public string hitObjName;
    public int CollectedCoins;
    public int killCount;
    public float finalDistance;
    public GameObject BgSoundObj, PowerupTimer;
    public Vector3 playerPosition;
    public bool isGamedEnded = false;
    //Vector3 originalBestDistancePosition;
    public int ShieldPowerCount;
    public bool shieldPoweActive = false;
    public int HelthCount = 3;
    public Material trailRender;
    public GameObject Helth1, Helth2, Helth3;
    public GameObject TapToStart, jumpText, attackText;
    public Material[] WorldMaterials;
    public Texture[] World1, World2, World3, World4;

    public PlayerController playerController;
    public GameObject dialogParent, mainParent, powerShowParent, gameoverParent, loadingParent, imgBestDistance;
    public Text txtCoins, txtKill, txtDistance, txtBestDistance, txtCurrentCoin, txtCurrentKill, txtCurrentDistance;
    public Animator iconPowerAnim, animGameOver;
    public Image currentIconPower;
    public Sprite[] iconPowers;

    void OnEnable()
    {
        sequence = Random.Range(1, 5);
        ChangeWorldTextures();
    }

    void Start()
    {
        Static = this;
        mainParent.SetActive(false);
        Time.timeScale = 1;
        ShieldPowerCount = PlayerPrefs.GetInt("ShieldCount", 0);

        iTween.ShakeRotation(TapToStart, iTween.Hash("amount", new Vector3(0.1f, 0.1f, 0.01f), "time", 0.3f, "Eesetype", iTween.EaseType.easeInOutBack, "looptype", iTween.LoopType.loop));
    }

    public void OnGameStart()
    {
        iTween.ScaleTo(TapToStart, iTween.Hash("scale", new Vector3(3f, 3.001f, 2.001f), "time", 0.5f, "Eesetype", iTween.EaseType.easeInOutBack));
        iTween.FadeTo(TapToStart, 0, 0.3f);
        TapToStart.SetActive(false);
        mainParent.SetActive(true);
    }

    RaycastHit hitObject;

    void Update()
    {
        if (HelthCount == 0)
        {

            ongameEnded();
        }

        switch (HelthCount)
        {
            case 1:
                Helth3.SetActive(false);
                Helth2.SetActive(false);
                break;
            case 2:
                Helth3.SetActive(false);

                break;
        }

        if (!isGamedEnded)
        {

            finalDistance = Mathf.RoundToInt(PlayerController.PlayerCurrentPosition.x);
            txtCurrentDistance.text = " Distance : " + finalDistance;
            txtCurrentCoin.text = "Coins : " + Mathf.RoundToInt(CollectedCoins);
            txtCurrentKill.text = "Kills : " + Mathf.RoundToInt(killCount);
        }
    }

    public void onPressButton(string name)
    {
        switch (name)
        {
            case "pause":
                Time.timeScale = 0;
                mainParent.SetActive(false);
                dialogParent.SetActive(true);
                break;
            case "attack":
                playerController.onShoot();
                break;
            case "jump":
                playerController.onJump();
                break;
            case "resume":
                dialogParent.SetActive(false);
                mainParent.SetActive(true);
                Time.timeScale = 1;
                break;
            case "menu":
                SceneManager.LoadScene("Mainmenu");
                break;
            case "retry":
                ShowLoading();
                break;
        }
    }

    public void UpdateInGameCoinCount()
    {
        CollectedCoins++;
        txtCurrentCoin.text = "Coins : " + CollectedCoins;
    }

    bool justOnce = false;

    public void ongameEnded()
    {
        if (justOnce)
        {
            return;
        }

        GameController.Static.StopInstantiate();
        justOnce = true;
        GameObject.Find("worldBackground").GetComponent<UVScrolling>().enabled = false;
        GameObject.Find("BgSound").GetComponent<AudioSource>().enabled = false;
        PlayerController.Static.speed = 0;

        GameObject.Find("Main Camera").GetComponent<Camerafollow>().enabled = false;
        GameController.Static.StopInstantiate();
        isGamedEnded = true;
        EnableFinalScoreUI();

        PlayerController.currentState = PlayerController.PlayerStates.dead;
    }

    public void EnableFinalScoreUI()
    {
        mainParent.SetActive(false);
        dialogParent.SetActive(false);
        powerShowParent.SetActive(false);
        ShowGameOverDialog();
    }

    public void ShowGameOverDialog()
    {
        bool newBest = false;
        gameoverParent.SetActive(true);
        txtCoins.text = "" + Mathf.RoundToInt(CollectedCoins);
        txtKill.text = "" + Mathf.RoundToInt(killCount);
        txtDistance.text = "" + Mathf.RoundToInt(finalDistance);

        if (PlayerPrefs.GetInt("bestDistance", 0) < finalDistance)
        {
            newBest = true;
            PlayerPrefs.SetInt("bestDistance", Mathf.RoundToInt(finalDistance));
        }

        txtBestDistance.text = "" + PlayerPrefs.GetInt("bestDistance");

        animGameOver.Play("showGameOver");

        if (newBest)
        {
            imgBestDistance.SetActive(true);
        }
        PlayerPrefs.SetInt("TotalCoins", PlayerPrefs.GetInt("TotalCoins", 0) + CollectedCoins);
    }

    public void DisableIngameUI()
    {
        PowerupTimer.SetActive(false);
        //ShieldPower.SetActive(false);
    }

    void ShowLoading()
    {
        gameoverParent.SetActive(false);
        loadingParent.SetActive(true);
        loadingParent.GetComponent<Animator>().Play("showLoading");
        Invoke("LoadLevel", 1f);
    }

    void LoadLevel()
    {
        SceneManager.LoadSceneAsync("gameplay");
    }

    int sequence = 1;

    public void ChangeWorldTextures()
    {
        if (sequence > 4)
            sequence = 1;


        switch (sequence)
        {
            case 1:
                for (int i = 0; i <= 3; i++)
                {
                    WorldMaterials[i].mainTexture = World1[i];
                }

                break;
            case 2:
                for (int i = 0; i <= 3; i++)
                {
                    WorldMaterials[i].mainTexture = World2[i];
                }

                break;
            case 3:
                for (int i = 0; i <= 3; i++)
                {
                    WorldMaterials[i].mainTexture = World3[i];
                }
                break;
            case 4:
                for (int i = 0; i <= 3; i++)
                {
                    WorldMaterials[i].mainTexture = World4[i];
                }

                break;


        }
        sequence++;
    }

    public void showPowerGunDisplay(int type)
    {
        currentIconPower.sprite = iconPowers[type - 2];
        powerShowParent.SetActive(true);
        GameController.Static.stopGameplay = true;
        iconPowerAnim.Play("iconPower");
        Invoke("stopPowerDisplay", 2.0f);
    }

    public void stopPowerDisplay()
    {
        Debug.Log("stop power");
        GameController.Static.stopGameplay = false;
        iconPowerAnim.Stop();
        powerShowParent.SetActive(false);
    }
}
