﻿using UnityEngine;

public class SoundController : MonoBehaviour
{
    public AudioClip ClickSound, CoinsCollectSound, powersound, JumpSound, Bullet1Sound, Bullet2Sound, Bullet3Sound, Bullet4Sound, LaserfireSound, EmemyDead;

    public static SoundController Static;
    public AudioSource[] audioSources;

    //public AudioSource scoreCount,bgSound ;

    private void Start()
    {
        Static = this;
    }

    // Update is called once per frame

    public void PlayCoinsCollectSound()
    {
        swithAudioSources(CoinsCollectSound);
    }

    public void PlayJumpSound()
    {
        swithAudioSources(JumpSound);
    }

    public void PlayEmemyDead()
    {
        swithAudioSources(EmemyDead);
    }

    public void PlayBullet1Sound()
    {
        swithAudioSources(Bullet1Sound);
    }

    public void PlayBullet2Sound()
    {
        swithAudioSources(Bullet2Sound);
    }

    public void PlayBullet3Sound()
    {
        swithAudioSources(Bullet3Sound);
    }

    public void PlayBullet4Sound()
    {
        swithAudioSources(Bullet4Sound);
    }

    public void Playpowersound()
    {
        swithAudioSources(powersound);
    }

    public void PlayClickSound()
    {
        swithAudioSources(ClickSound);
    }

    public void PlayLaserfireSound()
    {
        swithAudioSources(LaserfireSound);
    }

    //public void StopSounds ()
    //{
    //	audio.Stop ();
    //}

    private void swithAudioSources(AudioClip clip)
    {
        if (audioSources[0].isPlaying)
        {
            audioSources[1].PlayOneShot(clip);
        }
        else audioSources[0].PlayOneShot(clip);
    }
}