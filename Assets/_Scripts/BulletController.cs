﻿using UnityEngine;

public class BulletController : MonoBehaviour
{
    // Use this for initialization
    //public GameObject gun5;
    public enum bulletTypes
    {
        gun1, gun2, gun3, gun4, gun5
    }

    public bulletTypes currentBulletType;
    private Transform thisTrans;
    public float speed_for_gun1, speed_for_gun3, speed_for_gun4;

    private void Start()
    {
        thisTrans = transform;
    }

    // Update is called once per frame
    private void Update()
    {
        switch (currentBulletType)
        {
            case bulletTypes.gun1:
            case bulletTypes.gun2:
                thisTrans.Translate(Vector3.right * speed_for_gun1 * Time.deltaTime);

                break;

            case bulletTypes.gun3:
                thisTrans.Translate(Vector3.right * speed_for_gun3 * Time.deltaTime);
                break;

            case bulletTypes.gun4:
                thisTrans.Translate(Vector3.right * speed_for_gun4 * Time.deltaTime);
                break;

            case bulletTypes.gun5:
                //			Invoke("Deactice",0.9f);
                //			thisTrans.Translate(Vector3.right*speed_for_gun5*Time.deltaTime);
                break;
        }
    }

    private int hitCount;

    private void OnCollisionEnter(Collision hit)
    {
        Debug.Log("collided with " + hit.collider.name);
        if (hit.collider.name.Contains("Enemy"))
        {
            switch (currentBulletType)
            {
                case bulletTypes.gun1:
                    print("bullet 1 hit");
                    SoundController.Static.PlayEmemyDead();
                    EnemyScript enemyScript = hit.collider.GetComponent<EnemyScript>();
                    if (enemyScript != null)
                    {
                        enemyScript.health--;
                        iTween.ShakePosition(hit.collider.gameObject, iTween.Hash("amount", new Vector3(0.2f, 0.2f, 0.2f), "time", 0.5f));
                        if (enemyScript.health <= 0) Destroy(hit.collider.gameObject);
                    }
                    Destroy(gameObject);
                    //	GameObject.Instantiate(GameController.Static.SmokeParticle,thisTrans.position+ Vector3.right,Quaternion.identity);
                    GameObject.Instantiate(GameController.Static.Explosion1, thisTrans.position + Vector3.right, Quaternion.identity);
                    InGameUIController.Static.killCount++;
                    break;

                case bulletTypes.gun2:
                    SoundController.Static.PlayEmemyDead();
                    //GameObject.Instantiate(GameController.Static.SmokeParticle,thisTrans.position+ Vector3.right,Quaternion.identity);
                    GameObject.Instantiate(GameController.Static.Explosion2, thisTrans.position + Vector3.right, Quaternion.identity);
                    Destroy(hit.collider.gameObject);
                    Destroy(gameObject);
                    InGameUIController.Static.killCount++;
                    break;

                case bulletTypes.gun3:
                    SoundController.Static.PlayEmemyDead();
                    //GameObject.Instantiate(GameController.Static.SmokeParticle,thisTrans.position+ Vector3.right,Quaternion.identity);
                    GameObject.Instantiate(GameController.Static.Explosion3, thisTrans.position + Vector3.right, Quaternion.identity);
                    Destroy(hit.collider.gameObject);
                    InGameUIController.Static.killCount++;
                    break;

                case bulletTypes.gun4:
                    SoundController.Static.PlayEmemyDead();
                    //GameObject.Instantiate(GameController.Static.SmokeParticle,thisTrans.position+ Vector3.right,Quaternion.identity);
                    GameObject.Instantiate(GameController.Static.Explosion4, thisTrans.position + Vector3.right, Quaternion.identity);
                    Destroy(hit.collider.gameObject);
                    hitCount++;
                    if (hitCount >= 2) Destroy(gameObject);
                    InGameUIController.Static.killCount++;
                    break;

                case bulletTypes.gun5:
                    SoundController.Static.PlayEmemyDead();
                    Destroy(hit.collider.gameObject);
                    //GameObject.Instantiate(GameController.Static.SmokeParticle,thisTrans.position+ Vector3.right,Quaternion.identity);
                    GameObject.Instantiate(GameController.Static.Explosion5, thisTrans.position + Vector3.right, Quaternion.identity);
                    break;
            }
        }
        if (hit.collider.name.Contains("platfrom"))
        {
            switch (currentBulletType)
            {
                case bulletTypes.gun1:
                case bulletTypes.gun2:
                case bulletTypes.gun3:
                case bulletTypes.gun4:
                    Destroy(gameObject);
                    GameObject.Instantiate(GameController.Static.Explosion6, thisTrans.position + Vector3.right, Quaternion.identity);
                    break;
            }
        }
    }
}